// lazyload config
var servername = "http://"+window.location.hostname+'::8000/';
var MODULE_CONFIG = {
    easyPieChart:   [ servername + 'admin-assets/libs/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js' ],
    sparkline:      [ servername + 'admin-assets/libs/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js' ],
    plot:           [ servername + 'admin-assets/libs/jquery/flot/jquery.flot.js',
                      servername + 'admin-assets/libs/jquery/flot/jquery.flot.resize.js',
                      servername + 'admin-assets/libs/jquery/flot/jquery.flot.pie.js',
                      servername + 'admin-assets/libs/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                      servername + 'admin-assets/libs/jquery/flot-spline/js/jquery.flot.spline.min.js',
                      servername + 'admin-assets/libs/jquery/flot.orderbars/js/jquery.flot.orderBars.js'],
    vectorMap:      [ servername + 'admin-assets/libs/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                      servername + 'admin-assets/libs/jquery/bower-jvectormap/jquery-jvectormap.css', 
                      servername + 'admin-assets/libs/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                      servername + 'admin-assets/libs/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js' ],
    dataTable:      [
                      servername + 'admin-assets/libs/jquery/datatables/media/js/jquery.dataTables.min.js',
                      servername + 'admin-assets/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                      servername + 'admin-assets/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
    footable:       [
                      servername + 'admin-assets/libs/jquery/footable/dist/footable.all.min.js',
                      servername + 'admin-assets/libs/jquery/footable/css/footable.core.css'
                    ],
    screenfull:     [
                     // servername + 'admin-assets/libs/jquery/screenfull/dist/screenfull.min.js'
                    ],
    sortable:       [
                      servername + 'admin-assets/libs/jquery/html.sortable/dist/html.sortable.min.js'
                    ],
    nestable:       [
                      servername + 'admin-assets/libs/jquery/nestable/jquery.nestable.css',
                      servername + 'admin-assets/libs/jquery/nestable/jquery.nestable.js'
                    ],
    summernote:     [
                      servername + 'admin-assets/libs/jquery/summernote/dist/summernote.css',
                      servername + 'admin-assets/libs/jquery/summernote/dist/summernote.js'
                    ],
    parsley:        [
                      servername + 'admin-assets/libs/jquery/parsleyjs/dist/parsley.css',
                      servername + 'admin-assets/libs/jquery/parsleyjs/dist/parsley.min.js'
                    ],
    select2:        [
                      servername + 'admin-assets/libs/jquery/select2/dist/css/select2.min.css',
                      servername + 'admin-assets/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
                      servername + 'admin-assets/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.4.css',
                      servername + 'admin-assets/libs/jquery/select2/dist/js/select2.min.js'
                    ],
    datetimepicker: [ 
                      servername + 'js/bootstrap-datetimepicker.css',
                      servername + 'admin-assets/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css',
                      servername + 'admin-assets/libs/js/moment/moment.js',
                      servername + 'admin-assets/libs/jquery/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
                    ],
    chart:          [
                      servername + 'admin-assets/libs/js/echarts/build/dist/echarts-all.js',
                      servername + 'admin-assets/libs/js/echarts/build/dist/theme.js',
                      servername + 'admin-assets/libs/js/echarts/build/dist/jquery.echarts.js'
                    ],
    bootstrapWizard:[
                      servername + 'admin-assets/libs/jquery/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js'
                    ],
    fullCalendar:   [
                      servername + 'admin-assets/libs/jquery/moment/moment.js',
                      servername + 'admin-assets/libs/jquery/fullcalendar/dist/fullcalendar.min.js',
                      servername + 'admin-assets/libs/jquery/fullcalendar/dist/fullcalendar.css',
                      servername + 'admin-assets/libs/jquery/fullcalendar/dist/fullcalendar.theme.css',
                      'scripts/plugins/calendar.js'
                    ],
    dropzone:       [
                      servername + 'admin-assets/libs/js/dropzone/dist/min/dropzone.min.js',
                      servername + 'admin-assets/libs/js/dropzone/dist/min/dropzone.min.css'
                    ]
  };
